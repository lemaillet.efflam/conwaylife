import conwaylife as cwl

import tkinter as tk

# Great answer for simple usage of tkinter to create a board game :
# https://stackoverflow.com/questions/4954395/create-board-game-like-grid-in-python
def getorigin(eventorigin):
    global x0,y0
    row = int(eventorigin.y / my_frame.size)
    col = int(eventorigin.x / my_frame.size)
    my_grid.present[row][col]= 1 if my_grid.present[row][col] == 0 else 1
    draw()

def resize(event):
    '''Redraw the board, possibly in response to window being resized'''
    try:
        xsize = int((event.width-1) / side)
        ysize = int((event.height-1) / side)
        my_frame.size = min(xsize, ysize)
    except:
        pass
    draw()

def next_step(event):
    my_grid.update()
    draw()

def draw():
    my_canvas.delete("square")
    color = color_dead
    for row in range(side):
        for col in range(side):
            x1 = (col * my_frame.size)
            y1 = (row * my_frame.size)
            x2 = x1 + my_frame.size
            y2 = y1 + my_frame.size
            color = color_alive if my_grid.present[row][col] == 1 else color_dead
            my_canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")


if __name__ == "__main__":
    root = tk.Tk()

    my_grid = cwl.Grid(20)
    my_grid.present[2][1]=1
    my_grid.present[3][1]=1
    my_grid.present[2][2]=1
    my_grid.present[3][2]=1

    my_grid.present[5][5]=1
    my_grid.present[5][6]=1
    my_grid.present[5][7]=1

    my_grid.present[6][6]=1
    my_grid.present[6][7]=1
    my_grid.present[6][8]=1
    # px for a element
    size = 32
    # length of side
    side = my_grid.size
    color_dead="white"
    color_alive="black"
    canvas_width = side * size
    canvas_height = side * size

    #1 Create a TK Frame :
    my_frame = tk.Frame(root)
    my_frame.pack(side="top", fill="both", expand="true", padx=4, pady=4)
    #create canvas in my frame
    my_canvas = tk.Canvas(my_frame, borderwidth=0, highlightthickness=0,
                                    width=canvas_width, height=canvas_height, background="bisque")

    my_canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)

    # this binding will cause a refresh if the user interactively
    # changes the window size
    my_canvas.bind("<Button-1>", getorigin)
    my_canvas.bind("<Configure>",resize)
    root.bind("<Key>", next_step)
    root.mainloop()


