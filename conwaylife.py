"""
 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
 2. Any live cell with more than three live neighbours dies, as if by overcrowding.
 3. Any live cell with two or three live neighbours lives on to the next generation.
 4. Any dead cell with exactly three live neighbours becomes a live cell.
 >> Neighbours include diagonaly adjacents
"""


class Grid:
    """ A 2d array of int"""
    def __init__(self,size) -> None:
        self.size=size
        self.present=[[ 0 for x in range(size)] for y in range(size)]

    def update(self):
        """ Update Cell life for next step"""
        future=[]
        for i in range(self.size):
            future_line=[]
            for j in range(self.size):
                current =self.present[i][j]
                neighbors=0
                #get alive neighbors
                for ni in range(-1,2):
                    for nj in range(-1,2):
                        #ignore self and out of bounds
                        if (ni != 0 or nj != 0) \
                          and i+ni > -1 and i+ni < self.size \
                          and j+nj > -1 and j+nj < self.size :
                            neighbors += self.present[i+ni][j+nj]
                #set future state for current
                current_future=0
                #survive
                if neighbors == 2 and current == 1:
                    current_future=1
                #survive or born
                elif neighbors == 3:
                    current_future=1
                future_line.append(current_future)
            future.append(future_line)
        self.present=future
    def __repr__(self):
        """ terminal print  """
        print('\n'.join([''.join(['{:4}'.format(item) for item in row])
      for row in self.present]))

